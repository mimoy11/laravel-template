# PHP, Laravel, MySQL, Apache, NPM, VueJS Template
This is a boilerplate for a development and production ready application using
PHP, Laravel, MySQL, Apache, NPM, VueJS Template.

This project should only be used for development purposes and somehow can be used in production as well but with few config modifications. 

This project uses the docker development template created [veevidify](https://github.com/veevidify/laravel-apache-docker)

## System Requirements
- Ubuntu  18.04 ~ 20 operating system 
- [Docker engine](https://docs.docker.com/engine/install/ubuntu/)
- [Docker compose](https://docs.docker.com/compose/install/)

## Installation

**Clone the repo:**

```
$ git clone git@gitlab.com:mimoy11/laravel-template.git
$ cd master
```

**Build the images and start docker:**
```bash
$ docker-compose up -d --build
```

**Install dependencies:**
```bash
$ docker-compose up -d --build
$ ./composer install
$ ./php-artisan migrate
$ ./npm install 
```

## Usage

**Running the application in development w/ watch mode**
```bash
$ docker-compose up -d --build
$ ./npm run watch
```

**Running the application in development w/o no watch mode**
```bash
$ docker-compose up -d --build
$ ./npm run development
```

**Running the application in production**
```bash
$ docker-compose up -d --build
$ ./npm run production
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIMOY](https://gitlab.com/mimoy11/)
