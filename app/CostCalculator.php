<?php
/**
 * This class is responsible for calculating the
 * probable cost of studies
 *
 * @author Arthemus Malicdem
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
use Carbon\CarbonPeriod;


class CostCalculator extends Model
{
    /**
     * Static variable for the constant RAM value.
     * As the business rule dictates that 1000 studies
     * has the equivalent of 500mb of RAM hence, we 
     * have to calculate by GB since studies are priced
     * per GB.
     *
     * Same rules applies to DISK usage but the equivalent
     * of 1 study is equal to 10mb of disk or 0.01 Gb
     */
    private static $RAM = 0.5;
    private static $DISK = 0.01;

    /**
     * This is the cost of RAM per hour $0.00533
     */
    private static $COST_OF_RAM_PER_HOUR = 0.00533;

    /**
     * This is the cost of DISK per hour $0.10
     */
    private static $COST_OF_DISK_PER_MONTH = 0.10;

    /**
     * Hours in a month
     */
    private static $HOURS_IN_MONTH = 730.001;

    /**
     * These are privately defined variables
     */

    // Studies count
    private $studies_count = 0;

    // Monthly percent grownth
    private $monthly_percent_growth = 0;

    // Months to forecast
    private $months_to_forecast = 0;


    /**
     * Public constructor
     */ 
    public function __construct(
        int $studies_count,
        float $monthly_percent_growth,
        int $months_to_forecast
    ) {
        $this->studies_count = $studies_count;
        $this->monthly_percent_growth = $monthly_percent_growth;
        $this->months_to_forecast = $months_to_forecast;
    }

    /**
     *
     * This is a private function that calculates for the
     * percentage growth per month
     */
    private function get_monthly_growth()
    {
        return (($this->monthly_percent_growth / 100) * $this->studies_count) 
               + $this->studies_count;
    }

    /**
     *
     * This is a private function that generates
     * the list of months based on the provided number
     * of months to forecast
     */
    private function get_forecasted_months()
    {
        $today = today();
        $this_year = $today->year . '-01-01';
        return CarbonPeriod::create(
            $this_year,
            '1 month',
            $this->months_to_forecast
        );
    }

    /**
     * This function calculates the cost of RAM per hour
     *
     * @param $studies (int) The number of studies per month
     * @return (float)
     */
    private function calculate_ram_usage_price(float $studies)
    {
        // Convert the monthly growth of studies ram usage to GB
        $gigabytes_value = ($studies * self::$RAM) / 1000;
        return $gigabytes_value * self::$COST_OF_RAM_PER_HOUR * self::$HOURS_IN_MONTH;
    }

    /**
     * Calculates disk storage per month
     *
     * @param $studies (int) The number of studies per month
     * @return (float)
     */
    private function calculate_disk_usage_price(float $studies)
    {
        // Convert the monthly growth of studies disk usage to GB
        $gigabytes_value = $studies * self::$DISK;
        return $gigabytes_value * self::$COST_OF_DISK_PER_MONTH;
    }

    /**
     * This function calculates the total incurred charges
     * of studies per month.
     *
     * @return array
     */
    public function calculate()
    {
        $dates = [];
        $period = $this->get_forecasted_months();

        foreach ($period as $month) {
            $key = $month->format('Y-m-d');

            // Let's store the studies_count in memory so that we can
            // remember the last study count value and increment
            // it by the passed study growth percentage.
            $this->studies_count = $this->get_monthly_growth();
            
            // Compute for the total ram usage and disk price then round it the sum
            // to the nearest 2 decimal places
            $total = round($this->calculate_ram_usage_price($this->studies_count) 
                   + $this->calculate_disk_usage_price($this->studies_count), 2);

            $dates[] = [
                'date'              => $month->format('M Y'),
                'number_of_studies' => number_format($this->studies_count),
                'month_cost'        => "$" . (string) number_format($total, 2, '.', ',')
             ];
        }
        
       return $dates; 
    }
}
