<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\CostCalculator;

class StudiesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // This should only be accessible with an authenticated user
    }


    /**
     * This controller method gets items from the studies table
     * given the primary key/identifier of a study record.
     *
     * This controller method only retrieves single record
     * 
     * @param $id The id of the study to find
     * @return App\Studies
     */
    public function get($id)
    {
        $validator = Validator::make(
            ['study_id' => $id],
            ['study_id' => 'integer'],
            ['integer'  => 'Only valid identifiers are accepted']
        );

        if ($validator->fails()) {
            return response()->json([
                "error"     => true,
                "message"   => $validator->errors(),
                "data"      => null
            ], 400);
        }

        return json_encode([
            "success"   => true,
            "data"      => []
        ]);
    }

    /**
     * This method retrives multiple values of studies records
     *
     * @return App\Studies
     */
    public function getAll(Request $requet)
    {
        return json_encode([
            "success"   => true,
            "data"      => []
        ]);
    }

    /**
     * This controller method creates a new record in the studies
     * table
     *
     * @param $request The request object context
     * Request Body 
     *  - data
     *      - study_name
     *      - study_growth
     *      - months_to_forecast
     * @param $studies App\Studies
     *
     * @return App\Studies
     */
    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'data'                           => 'required|array',
            'data.study_per_day'             => 'required|integer',
            'data.study_growth_per_month'    => 'required|numeric',
            'data.months_to_forecast'        => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error"     => true,
                "message"   => $validator->errors(),
                "data"      => null
            ], 400);
        }

        $request_body           = $request->all()['data'];
        $study_count            = $request_body['study_per_day'];
        $months_to_forecast     = $request_body['months_to_forecast'];
        $monthly_percent_growth = $request_body['study_growth_per_month'];
        
        // Use the class to calculate forecasted costs
        $forecast = new CostCalculator(
            $study_count,
            $monthly_percent_growth,
            $months_to_forecast
        );

        return json_encode([
            "success"   => true,
            "data"      => $forecast->calculate()
        ]);
    }
}
