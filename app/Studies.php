<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Studies extends Model
{
    /**
     * The table associated with this model
     *
     * @var string
     */
    protected $table = 'studies';

    /**
     * 
     * These are the fillable properties that can 
     * be used to CRUD database columns
     *
     */
    protected $fillable = array(
        'number_of_study_growth',
        'number_of_study_per_month',
        'study_per_day',
        'ram_usage',
        'disk_usage'
    );
}
