<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyPrices extends Model
{
    /**
     * The table associated with this model
     *
     * @var string
     */
    protected $table = 'study_prices';

    /**
     * 
     * These are the fillable properties that can 
     * be used to CRUD database columns
     *
     */
    protected $fillable = array(
        'study_id',
        'size',
        'unit',
        'type',
        'price'
    );
}
