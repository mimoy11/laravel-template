<?php

use Illuminate\Database\Seeder;

class PricesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prices')->insert([
            'item_type'     => "MEM",
            'price'         => 0.00553,
            'term'          => 'HOURLY',
            'unit'          => 'GB',
            'measurement'   => 1
        ]);
        
        DB::table('prices')->insert([
            'item_type'     => "DISK",
            'price'         => 0.10,
            'term'          => 'MONTHLY',
            'unit'          => 'GB',
            'measurement'   => 1
        ]);
    }
}
