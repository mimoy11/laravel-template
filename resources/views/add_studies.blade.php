<form>
  <div class="form-group">
    <label for="study-name-txt">Name</label>
    <input type="text" class="form-control" id="study-name-txt" aria-describedby="study-name-aria" placeholder="Enter Study Name">
  </div>
  <div class="form-group">
    <label for="study-growth-txt">Number of Study Growth per Month in %</label>
    <input type="text" class="form-control"
    id="study-growth-txt" placeholder="Study Growth %">
  </div>
  <div class="form-group">
    <label for="number-of-months-txt">Number of Months to Forecast</label>
    <input type="text" class="form-control"
    id="number-of-months-txt" placeholder="Number Forecast Month">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
